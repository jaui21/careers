# Careers

## Extension Functionalities:
- Separate Careers section in your M2
- Create a category, assign career to category
- View careers by category
- Support multiple-themes,admin users can choose different THEMES 
- Display career view in Grid or List view
- Redirection to Linkedin job post links 
- Enable and disable career post from backend
