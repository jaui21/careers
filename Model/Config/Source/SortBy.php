<?php

/**
 * Jaui_Careers
 *
 * PHP version 7.0
 *
 * @category Magento2-module
 * @package  Jaui_Careers
 * @author   Jaui21
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */

namespace Jaui\Careers\Model\Config\Source;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class SortBy
 *
 * @category Magento2-module
 * @package  Jaui\Careers\Model\Config\Source
 * @author   Jaui21
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */
class SortBy implements OptionSourceInterface
{
    /**
     * To Option Array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }

    /**
     * Get All Options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = [
                    'value' => $index,
                    'label' => $value
                ];
        }

        return $result;
    }

    /**
     * Get Option Array
     *
     * @return array
     */
    public static function getOptionArray()
    {
        return
               [
                   \Jaui\Careers\Api\Data\CareersInterface::POSITION
                   => __('Position'),
                   \Jaui\Careers\Api\Data\CareersInterface::TITLE
                   => __('Title'),
                   \Jaui\Careers\Api\Data\CareersInterface::CREATED_AT
                   => __('Date')
               ];
    }
}