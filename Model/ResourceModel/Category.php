<?php

/**
 * Jaui_Careers
 *
 * PHP version 7.0
 *
 * @category Magento2-module
 * @package  Jaui_Careers
 * @author   Jaui21
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */

namespace Jaui\Careers\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Category
 *
 * @category Magento2-module
 * @package  Jaui\Careers\Model\ResourceModel
 * @author   Jaui21
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */
class Category extends AbstractDb
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('jaui_careers_category', 'id');
    }
}