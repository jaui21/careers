<?php

/**
 * Jaui_Careers
 *
 * PHP version 7.0
 *
 * @category Magento2-module
 * @package  Jaui_Careers
 * @author   Jaui21
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */

namespace Jaui\Careers\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface CareersSearchResultsInterface
 *
 * @category Magento2-module
 * @package  Jaui\Careers\Api\Data
 * @author   Jaui21
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */
interface CareersSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get items list.
     *
     * @return \Magento\Cms\Api\Data\BlockInterface[]
     */
    public function getItems();

    /**
     * Set items list.
     *
     * @param \Magento\Cms\Api\Data\BlockInterface[] $items Set Items
     *
     * @return $this
     */
    public function setItems(array $items);
}