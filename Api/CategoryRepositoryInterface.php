<?php

/**
 * Jaui_Careers
 *
 * PHP version 7.0
 *
 * @category Magento2-module
 * @package  Jaui_Careers
 * @author   Jaui21
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */

namespace Jaui\Careers\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface CategoryRepositoryInterface
 *
 * @category Magento2-module
 * @package  Jaui\Careers\Api
 * @author   Jaui21
 * @license  OSL <https://opensource.org/licenses/OSL-3.0>
 * @link     
 */
interface CategoryRepositoryInterface
{
    /**
     * Save
     *
     * @param Data\CategoryInterface $item Item
     *
     * @return mixed
     */
    public function save(Data\CategoryInterface $item);

    /**
     * Get By Id
     *
     * @param int $entityId Entity
     *
     * @return mixed
     */
    public function getById($entityId);

    /**
     * Get List
     *
     * @param SearchCriteriaInterface $searchCriteria SearchCriteria
     *
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete
     *
     * @param Data\CategoryInterface $item Item
     *
     * @return mixed
     */
    public function delete(Data\CategoryInterface $item);

    /**
     * Delete By Id
     *
     * @param int $entityId Entity
     *
     * @return mixed
     */
    public function deleteById($entityId);
}